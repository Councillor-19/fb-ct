<?php

namespace Tests\Feature;

use App\User as User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetAuthTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_be_fetched()
    {
        $this->actingAs($user = factory(User::class)->create(), 'api');

        $response = $this->get('/api/auth-user');

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => [
                         'id' => $user->id,
                         'attributes' => [
                             'name' => $user->name,
                         ]
                     ],
                     'links' => [
                         'self' => url('/users/' . $user->id)
                     ]
                 ]);
    }
}
